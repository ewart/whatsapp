<?php

class Whatsapp
{
    private $config;
    private $from;
    private $number;
    private $id;
    private $nick;
    private $password;
    private $contacts = array();
    private $inputs;
    private $messages;
    private $wa;
    private $connected;
    private $waGroupList;
    

    public function __construct(array $config)
    {
        $this->config = $config;
        $this->number = $this->config['fromNumber'];
        $this->id = $this->config['id'];
        $this->nick = $this->config['nick'];
        $this->password = $this->config['waPassword'];
        $this->wa = new WhatsProt($this->number, $this->id, $this->nick, false);
        $this->wa->eventManager()->bind('onGetMessage', array($this, 'processReceivedMessage'));
        $this->wa->eventManager()->bind('onConnect', array($this, 'connected'));
        $this->wa->eventManager()->bind('onGetGroups', array($this, 'processGroupArray'));
    }
    /**
     * Sets flag when there is a connection with WhatsAPP servers.
     *
     * @return void
     */
    public function connected()
    {
        $this->connected = true;
    }
 
    /**
     * Process the latest request.
     *
     * Decide what course of action to take with the latest
     * request/post to this script.
     *
     * @return void
     */
    public function process()
    {
        $this->sendMessageCustom();
    }

    public function __destruct()
    {
        if (isset($this->wa) && $this->connected) {
            $this->wa->disconnect();
        }
    }
    /**
     * Connect to Whatsapp.
     *
     * Create a connection to the whatsapp servers
     * using the supplied password.
     *
     * @return boolean
     */
    private function connectToWhatsApp()
    {
        if (isset($this->wa)) {
            $this->wa->connect();
            $this->wa->loginWithPassword($this->password);
            return true;
        }
        return false;
    }


    private function sendMessageCustom()  {
        // copy("http://www.beatthetraffic.com/data/camera.aspx?id=navteq:95233", "knight.jpg");
        // copy("http://www.beatthetraffic.com/data/camera.aspx?id=navteq:87067", "oakbridge.jpg");

        $to = "16047229158";

        $this->connectToWhatsApp();
        // $this->wa->sendMessageComposing($to);
        // $this->wa->sendMessage($to, "yup sounds great!");   
        $this->wa->sendMessageImage($to, "http://images.drivebc.ca/bchighwaycam/pub/cameras/66.jpg");
        sleep(5);
        // $this->wa->sendMessageImage($to, "oakbridge.jpg");
        // unlink("knight.jpg"); unlink("oakbridge.jpg");
    }
 
    /**
    * Process inbound text messages.
    *
    * If an inbound message is detected, this method will
    * store the details so that it can be shown to the user
    * at a suitable time.
    *
    * @param string $phone The number that is receiving the message
    * @param string $from  The number that is sending the message
    * @param string $id    The unique ID for the message
    * @param string $type  Type of inbound message
    * @param string $time  Y-m-d H:m:s formatted string
    * @param string $name  The Name of sender (nick)
    * @param string $data  The actual message
    *
    * @return void
    */
    public function processReceivedMessage($phone, $from, $id, $type, $time, $name, $data)
    {
     $matches = null;
     $time = date('Y-m-d H:i:s', $time);
     if (preg_match('/\d*/', $from, $matches)) {
         $from = $matches[0];
     }
     $this->messages[] = array('phone' => $phone, 'from' => $from, 'id' => $id, 'type' => $type, 'time' => $time, 'name' => $name, 'data' => $data);
    }
}

?>